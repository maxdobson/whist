import Vue from 'vue';
import Vuex from 'vuex';

// This is the sole Vuex store used for the application, and handles pretty much all client side data.
// All of the socket messages are handled here and mutuate the client data.

Vue.use(Vuex);

import { ToastProgrammatic as Toast } from 'buefy'
export default new Vuex.Store({
  state: {
    count: 0,
    // SOCKETS
    conn: {
        connected: false,
        error: '',
        message: '',
    },
    code: '',
    trumps: '',
    currentround: '',
    players : [],
    outplayers : [],
    hand : [],
    wontrick : '',
    dealer : '',
    gamestate: 'welcome',
    selectedcard: 100, // Holds the ID of the selected card.
    message: 'Get started',
    admin: '',
    active: true,
    user: {
        username: '',
    },
    suitindex: ["\u2665", "\u2663", "\u2660", "\u2666"],  // "\u2660"  ['H','C','S','D']
    numberindex : [2,3,4,5,6,7,8,9,10,"J","Q","K","A"],
  },
  mutations: {
    // Client side mutations
    // ----------------------
    set_username(state, newUsername) {
      state.user.username = newUsername;
    },
    
    set_gamestate(state, gamestate) {
      state.gamestate = gamestate;
    },

    set_selectedcard(state, cardindex) {
      state.selectedcard = cardindex;
    },

    // Web socket events from server (SOCKET_EVENTNAME)
    //--------------------------------------------------
    SOCKET_CONNECT(state) {
      state.conn.connected = true
    },
    SOCKET_RECONNECT(state) {
      // Not sure if this ever fires - check and potentially delete
      state.conn.connected = true;
      console.log('SOCKET_RECONNECT');
    },
    SOCKET_DISCONNECT(state) {
      state.conn.connected = false
    },
    SOCKET_RECONNECT_ASKROOM(state, message) {
      // Not sure this ever fires - check and potentially delete
      state.conn.connected = true;
      console.log(message);
      this._vm.$socket.emit('refreshGameData', {'code': state.code });
    },

    SOCKET_NEWJOINER(state, message) {
      // Not sure this ever first - check and potentially delete
      var data = JSON.parse(message)
      console.log(data)
      state.code = data.code;
      state.gamestate = data.gamestate;
      state.trumps = data.trumps;
      state.currentround = data.currentround;
      state.players = data.players;
      state.outplayers = data.outplayers;
      state.hand = data.hand;
      state.current_turn = data.current_turn;
      state.message = data.message;
      state.active = data.active;
      state.wontrick = data.wontrick;
      state.dealer = data.dealer;
      state.calling_trumps = data.calling_trumps;
      state.tiebreak = data.tiebreak;
      state.admin = data.admin
    },
    SOCKET_GAMEUPDATE(state, message) {
      // Handles the main update message from the server.  Updates almost all client variables.
      var data = JSON.parse(message);
      console.log(data);
      state.code = data.code;
      state.gamestate = data.gamestate;
      state.trumps = data.trumps;
      state.currentround = data.currentround;
      state.players = data.players;
      state.outplayers = data.outplayers;
      Vue.set(state, 'hand', data.hand);
      state.message = data.message;
      state.active = data.active;
      state.wontrick = data.wontrick;
      state.dealer = data.dealer;
      state.calling_trumps = data.calling_trumps;
      state.tiebreak = data.tiebreak;
      state.admin = data.admin;
    },
    SOCKET_JOINFAILED(state, message) {
      // Handles when the server fails to add a client to a game room.
      var failedcode = JSON.parse(message).code;
      Toast.open({ message: "Code not recognised: " + failedcode, type: "is-danger"});
    },    
    SOCKET_TOASTMESSAGE(state, message) {
      // Generic function used to show 'toast' messages from server with given text and style.
      var toastmessage = JSON.parse(message).message;
      var toaststyle = JSON.parse(message).style;
      Toast.open({ message: toastmessage, type: toaststyle});
    },
    SOCKET_ROOM_CREATED(state, message) {
      // Todo: This is used, but might be able to use the generic GAMEUPDATE instead.
      var data = JSON.parse(message)
      state.code = data.code;
    },
    SOCKET_ERROR(state, message) {
      // Todo - this isn't used (but probably should be...)
      state.conn.error = message.error
    },
  }

  // NB: There are no actions used, only mutations.  If strange behaviour, might be something to investigate.
});
