#-----------
# game.py
#-----------
# Contains the game logic and classes for game objects.

import random

class Card(object):
    def __init__(self, suit, number, id, value=None, eligible=True, winning=False):
        self.suit = suit # Holds the suit (as an integer, 0-3)
        self.number = number # Holds the number of the card (as an integer 0-12)
        self.value = value # Used to store a number valuing the card for comparison purposes
        self.eligible = eligible # Boolean flag for whether the card is elgible to be played.
        self.id = id # A unique ID number for the card
        self.winning = winning # Boolean flag used to track whether the card is winning a hand.


class Deck(object):
    # Object used to contain any number of cards, and provide functions to manipulate them.
    def __init__(self, cards=None):
        self.cards = cards or []

    def shuffle(self):
        # Shuffles list in place.
        random.shuffle(self.cards) 

    def remove(self, position='top'):
        # Removes and returns a card from the deck. Takes an argument specifying where to draw from.
        if self.count() == 0:
            return None
        if position == 'top':
            pos = 0
        elif position == 'bottom':
            pos = len(self.cards) - 1
        elif position == 'random':
            pos = random.randint(0, len(self.cards) - 1)
        else:
            pos = position
        return self.cards.pop(pos)

    def add(self, card, position='top'):
        # Adds a card into the deck. Takes an extra argument specifying where to place it.
        if position == 'top':
            self.cards.insert(0, card)
        elif position == 'bottom':
            self.cards.append(card)
        elif position == 'random':
            pos = random.randint(0, len(self.cards) - 1)
            self.cards.insert(pos, card)

    def order(self):
        # Sorts the cards in the list, suit by suit in numerical ascending order.
        self.cards.sort(key=lambda x: ((x.suit*13) + x.number), reverse=False)

    def count(self):
        return len(self.cards)

    def get_cardposition_byid(self, cardid):
        # Helper function to get the position of a card.
        cardindex = [card.id for card in self.cards].index(cardid)
        return cardindex

    def __str__(self):
        # String representation of all cards in the hand.
        # Todo - could make it reflect the label on the cards themselves, rather than zero indexed references.
        x = []
        for card in self.cards:
            x.append('S{}N{}'.format(card.suit, card.number))
        return ', '.join(x)

    def gen_standard_deck(self):
        # Generates a standard deck of playing cards (without jokers)
        cards = []
        count = 0
        for suit in range(4):
            for number in range(13):
                cards.append(Card(suit, number, count))
                count += 1
        self.cards = cards

class Player(object):
    # Used to track individual players and provide functions to assist.
    def __init__(self, id, name, active=False, sessionid=None, connected=True, admin=False, tricks=0,
                 calling_trumps=False, hand=None, playedcard=None, colour=None,
                 start_turn_with=False, wontrick=False, ingame=True, dealer=False):
        self.number = None  # Intended to hold the player number, but may not be used? (Todo)
        self.id = id # Unique ID for each player.  Uses the SOCKET ID.  Note that this can change if reconnect!
        self.sessionid = sessionid # The sessionID for the player.  This should persist between refreshes.
        self.name = name # Username for the player
        self.active = active # Boolean for whether then player can act or not.
        self.connected = connected # Boolean for whether the client is connected.
        self.admin = admin # Boolean for whether the player is the admin (in charge of starting the game etc)
        self.tricks = tricks # Stores the number of tricks won in a round.
        self.hand = hand or Deck() # a Deck of cards for the players hand.
        self.colour = colour # Todo - remove.  This was intended to have player colours, but not needed.
        self.calling_trumps = calling_trumps # A boolean flag for whether calling trumps
        self.playedcard = playedcard # Holds a Card object for the card played in a round
        self.wontrick = wontrick # A boolean flag for whether have won the current round
        self.start_turn_with = start_turn_with # A boolean flag for whether starting the turn.
        self.ingame = ingame # Todo - remove.  Don't think this is actually used.
        self.dealer = dealer # Boolean flag for whether are the dealer.

class Whist(object):
    def __init__(self, drawpile, code, state='lobby', currentround=0, players=None, outplayers=None, tiebreak=None):
        self.name = 'Whist' # Name for the game. Not currently used but might be useful in future.
        self.colourlist = ['Red', 'Blue', 'Yellow', 'Green', 'Purple',
                           'Orange', 'Pink', 'Grey', 'Black'] # List of player colours - unused, todo remove.
        self.roundcards = {1: 7, 2: 6, 3: 5, 4: 4, 5: 3, 6: 2, 7: 1} # Standard number of cards in each round.
        self.numbers = [2, 3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K", "A"] # Number representation of each card. Todo unused?
        self.suits = ['C', "S", "D", "H"] # String representation of each suit. Todo unused?
        self.code = code # The code identifying the room.
        self.state = state  # Records the state/phase of the game (e.g. lobby, game etc)
        self.players = players or [] # List of players 
        self.drawpile = drawpile # The deck of cards that will be drawn from
        self.trumps = None # Integer representation of the suit that is trumps.
        self.currentsuit = None # Integer representation of the suit for the current hand
        self.currentround = currentround # Integer for the round number.
        self.activemessage = None # Message displayed to player due to act
        self.inactivemessage = None # Message displayed to player(s) not due to act
        self.tiebreak = tiebreak # Can hold a dict describing the outcome of a tiebreak for trumps.
        self.outplayers = outplayers or [] # List of players who are out of the game.

    @property
    def all_players(self):
        return self.players + self.outplayers

    def update_socket_from_session(self, sessionid, socketid):
        # Helper function to update the socket id based on the session id.
        # The session id should indentify the player consistently (even if they refresh browser,
        # or go to a differnet website).  The socketid changes each connection, so this function
        # must be called to keep the socketid in sync with the player.
        # Loops through every player (in game or out) to find the session id, and updates if found.
        for player in self.players:
            if player.sessionid == sessionid:
                player.id = socketid
        for player in self.outplayers:
            if player.sessionid == sessionid:
                player.id = socketid

    def out_player(self, playerid):
        # For a given playerid, identifies that player and moves them from players to outplayers.
        # Also sets them as inactive.
        playerindex, playerposition = self.get_player_position(playerid)
        outplayer = self.players.pop(playerindex)
        outplayer.active = False
        self.outplayers.append(outplayer)

    def mangage_connected_player(self, playerid, connected=True):
        # Helper function intended to take a playerid and set their connection status.  Todo - unused?
        for player in self.players:
            if player.id == playerid:
                player.connected = connected
        for player in self.outplayers:
            if player.id == playerid:
                player.connected = connected

    def add_player(self, player, watching=False):
        # Adds a provided player to the game, either playing or out of the game (watching)
        player.colour = self.colourlist.pop(0) # This isn't used - todo delete?
        if watching:
            self.outplayers.append(player)
        else:
            self.players.append(player)

    def add_watchplayer(self, player):
        # Seems unlikely to be used - todo delete?
        player.colour = self.colourlist.pop(0)
        self.outplayers.append(player)

    def order_players(self, startingplayerid, settofirstplayer=True):
        # Orders players in the players list - puts the startingplayer id at the top, and
        # moves the list around so that the players are still in the same order.
        # Also takes a parameter to set the first player as being active, e.g. if starting a new hand.
        startplayerposition = [p.id for p in self.players].index(startingplayerid)
        if startplayerposition != 0:
            self.players = self.players[startplayerposition:] + self.players[:startplayerposition]
        if settofirstplayer:
            self.players[0].active = True

    def choose_random_player(self):
        # returns the id of a player at random
        return random.choice(self.players).id

    def set_active_player(self, playerid):
        # Helper function that sets only one player active based on an id.
        for player in self.players:
            if player.id == playerid:
                player.active = True
            else:
                player.active = False

    def get_player_position(self, playerid):
        # Helper function to get a player index by id, and also a handy description of
        # where they are in the hand (first/middle/last).  Returns tuple.
        playerindex = [x.id for x in self.players].index(playerid)
        if playerindex == 0:
            return (playerindex, 'first')
        elif playerindex == len(self.players) - 1:
            return (playerindex, 'last')
        else:
            return (playerindex, 'middle')


    def decide_next_trumps(self):
        # Allocates the next player to call trumps.
        # Sets that player, and returns an object describing a tie break if
        # necessary.
        maxtricks = max([p.tricks for p in self.players]) # Gets top score
        topplayers = [] # Compiles list of dict players with top scorers
        for player in self.players:
            if player.tricks == maxtricks:
                topplayers.append({'id': player.id, 'name': player.name})
        if len(topplayers)== 1: # If only one playayer, just set them.
            self.set_trump_caller(topplayers[0]['id'])
            self.tiebreak = None
        else: # or run a tiebreak.
            self.tiebreak = {}
            self.tiebreak['cutcards'] = []
            valuelist = list(range(13)) # Take a list of the 13 card numbers.  Effectively one suit only.  (Avoids
            #                             complicating things with the possibility of drawing the same card, different suit).
            random.shuffle(valuelist) # Shuffle them
            counter = 0
            maxvalue = 0
            maxname = None
            maxid = None
            for player in topplayers: # For each of the players with the top scor
                tb = {}
                tb['id'] = player['id']
                tb['name'] = player['name']
                tb['number'] = valuelist[counter] # Take the next value from the shuffled card number list.
                tb['suit'] = random.randrange(4) # Allocate a suit at random (it doesn't affect the outcome)
                if tb['number'] > maxvalue:  
                    maxvalue = tb['number']
                    maxname = tb['name']
                    maxid = tb['id']
                self.tiebreak['cutcards'].append(tb) # This line and next set the tiebrreak description dict.
                self.tiebreak['winnername'] = maxname
                counter += 1
            self.set_trump_caller(maxid)
        
    def next_round(self):
        # Advances the game to the next round
        self.state = 'game'
        self.currentround += 1
        if self.currentround == 1: # For the first round, set trumps and player order at random.
            self.trumps = random.choice([0,1,2,3])
            print('Trumps chosen as {}'.format(self.suits[self.trumps]))
            self.order_players(self.choose_random_player())
            self.set_dealer(self.players[0].id)
            self.deal_cards()
            self.set_active_player(self.players[0].id)
        else: # For all other rounds, show the status screen and set up the new round.
           self.state = 'gamestatus'
           self.trumps = None
           self.remove_out_players()
           self.next_dealer()
           self.decide_next_trumps()

    def set_trump_caller(self, playerid=None):
        # Helper function to set the trump caller based on provided id.
        for player in self.players:
            if player.id == playerid:
                player.calling_trumps = True
            else:
                player.calling_trumps = False


    def set_dealer(self, playerid=None):
        # Helper function to set the dealer baed on provided id.
        for player in self.players:
            if player.id == playerid:
                player.dealer = True
            else:
                player.dealer = False

    def next_dealer(self):
        # Sets the next dealer in turn.
        if True in [x.dealer for x in self.players]: # If can find current dealer, set it to the next in list.
            dealerindex = [x.dealer for x in self.players].index(True)
            if dealerindex < len(self.players) - 1:
                newdealerindex = dealerindex + 1
            else:
                newdealerindex = 0
            self.set_dealer(self.players[newdealerindex].id)
        else:
            newdealerindex = 0 # Cant find a dealer, so just set the first player.  Possibly dealer out or game over?
            self.set_dealer(self.players[newdealerindex].id) # Todo - dealer allocation should be more robust.


    def remove_out_players(self):
        outlist = [player.id for player in self.players if player.tricks == 0]
        
        # Give free rides (for first go, and randomly with 33% chance in round 2)
        if (self.currentround - 1) == 1:
            return False
        if (self.currentround -1) == 2:
            grandad = random.randrange(3)
            if grandad == 1:
                return False

        for playerid in outlist:
            self.out_player(playerid)
        if len(self.players)==1:
            winnningplayername = self.players[0].name
            if self.players[0].admin == True:
                self.players[0].active = True
            else:
                self.players[0].active = False
            for player in self.outplayers:
                if player.admin == True:
                    player.active = True
                else:
                    player.active = False
            self.activemessage = '{} won! Restart?'.format(winnningplayername)
            self.inactivemessage = '{} won!'.format(winnningplayername)
            print('END OF THE GAME!!!!!!')            

    def deal_cards(self):
        self.drawpile.shuffle()
        numberofplayers = len(self.players)
        normalroundcards = self.roundcards[self.currentround]
        if numberofplayers * normalroundcards > 52:
            cardstodeal = int(52 / numberofplayers)
        else:
            cardstodeal = normalroundcards

        for player in self.players:
            for _ in range(cardstodeal):
                newcard = self.drawpile.remove()
                player.hand.add(newcard)
                print('Dealt {} to {}'.format(newcard.number, player.name))
            player.hand.order()

    def card_played(self, playerid, cardid):
        # Play the actual card
        playerindex, playerposition = self.get_player_position(playerid)
        cardindex = self.players[playerindex].hand.get_cardposition_byid(cardid)
        targetcard = self.players[playerindex].hand.remove(cardindex)
        self.players[playerindex].playedcard = targetcard
        if playerposition == 'first':
            # Set eligible cards for the round
            self.currentsuit = targetcard.suit
            for player in self.players:
                if player.id == playerid:
                    for card in player.hand.cards:
                        card.eligible = False
                else:
                    cardtofollowsuit = False
                    for card in player.hand.cards:
                        card.eligible = True
                        if card.suit == self.currentsuit:
                            cardtofollowsuit = True
                    if cardtofollowsuit:
                        for card in player.hand.cards:
                            if card.suit != self.currentsuit:
                                card.eligible = False
            self.players[playerindex].active = False
            self.inactivemessage = '{} to play'.format(self.players[playerindex + 1].name)
            self.players[playerindex + 1].active = True
            self.activemessage = 'Your turn'
            
        if playerposition == 'middle':
            self.players[playerindex].active = False
            self.inactivemessage = '{} to play'.format(self.players[playerindex + 1].name)
            self.players[playerindex + 1].active = True
            self.activemessage = 'Your turn'

        self.update_card_values()
        self.update_leading_card()

        if playerposition == 'last':
            for player in self.players:
                for card in player.hand.cards:
                    card.eligible = False
                
                if player.playedcard.winning:
                    player.active = True
                    player.wontrick = True
                    self.inactivemessage = '{} won the trick'.format(player.name)
                else:
                    player.active = False
                    player.wontrick = False
            self.activemessage = 'You won!'


    def next_trick(self, playerid):
        print('HAND:')
        print(self.players[0].hand.cards)
        print(type(self.players[0].hand))
        if not self.players[0].hand.cards:
            print('NEXT ROUND!!')
            for player in self.players:
                if player.wontrick:
                    player.tricks += 1
            self.next_round()
        else:
            for player in self.players:
                player.playedcard = None
                if player.wontrick:
                    player.tricks += 1
                    player.active = True
                    self.inactivemessage = '{} to lead'.format(player.name)
                    for card in player.hand.cards:
                        card.eligible = True
                else:
                    player.active = False
                    for card in player.hand.cards:
                        card.eligible = False
            for player in self.players:
                player.wontrick = False
            self.activemessage = 'Start next trick'
            self.order_players(playerid)

    def set_trumps(self, suitindex):
        self.trumps = suitindex
        for player in self.players:
            player.active = False
        self.players[0].active = True
        self.activemessage = 'Lead the trick'
        self.inactivemessage = '{} to lead'.format(self.players[0].name)
        for card in self.players[0].hand.cards:
            card.eligible = True

    def update_card_values(self):
        for player in self.players:
            if player.playedcard:
                if not player.playedcard.value:
                    if player.playedcard.suit == self.trumps:
                        player.playedcard.value = (player.playedcard.number + 1)+20
                    elif player.playedcard.suit == self.currentsuit:
                        player.playedcard.value = player.playedcard.number + 1
                    else:
                        player.playedcard.value = 0
                        
    def update_leading_card(self):
        currentmax = 0
        for player in self.players:
            if player.playedcard:
                if player.playedcard.value > currentmax:
                    currentmax = player.playedcard.value
        for player in self.players:
            if player.playedcard:
                if player.playedcard.value == currentmax:
                    player.playedcard.winning = True
                else:
                    player.playedcard.winning = False

    def clear_previous_hand(self):
        newdrawpile = Deck()
        newdrawpile.gen_standard_deck()
        newdrawpile.shuffle()
        self.drawpile = newdrawpile
        self.deal_cards()
        self.state = 'game'
        self.currentsuit = None
        self.tiebreak = None
        self.activemessage = 'Call trumps'
        for player in self.players:
            player.playedcard = None
            player.tricks = 0
            player.wontrick = False
            if player.calling_trumps:
                print('PLAYER {} calling trumps'.format(player.name))
                player.active = True                
                self.inactivemessage = '{} to call trumps'.format(player.name)
                for card in player.hand.cards:
                    card.eligible = False
            else:
                player.active = False

            for card in player.hand.cards:
                card.eligible = False

        dealerid = None
        for player in self.players:
            if player.dealer:
                dealerid = player.id
        self.order_players(dealerid, settofirstplayer=False)

    def get_active_playername(self):
        for player in self.players:
            if player.active:
                return player.name

    def render_all_dicts(self):
        for player in self.players:
            print('***** {} *****'.format(player.name))
            print(self.render_dict(player.id))

    def render_dict(self, playerid):
        if playerid in [p.id for p in self.players]:
            playerindex = [p.id for p in self.players].index(playerid)
        else:
            playerindex = None
        response = {}
        response['code'] = self.code
        response['gamestate'] = self.state
        response['trumps'] = self.trumps
        response['currentround'] = self.currentround
        response['tiebreak'] = self.tiebreak
        playerlist = []
        for player in self.players:
            if player.playedcard:
                playedcard = {'suit': player.playedcard.suit,
                              'number': player.playedcard.number,
                              'value': player.playedcard.value,
                              'id':  player.playedcard.id,
                              'winning': player.playedcard.winning}
            else:
                playedcard = None
            playerlist.append({'name': player.name, 'active': player.active,
                              'tricks': player.tricks, 
                              'playedcard': playedcard})
            if player.id == playerid:
                response['calling_trumps'] = player.calling_trumps
                response['wontrick'] = player.wontrick
                response['active'] = player.active
                response['dealer'] = player.dealer
                response['admin'] = player.admin
                if player.active:
                    response['message'] = self.activemessage
                else:
                    response['message'] = self.inactivemessage    
        response['players'] = playerlist

        outplayerlist = []
        for player in self.outplayers:
            outplayerlist.append({'name': player.name, 'id': player.id})
            if player.id == playerid:
                response['calling_trumps'] = False
                response['wontrick'] = False
                response['active'] = False
                response['dealer'] = False
                response['admin'] = player.admin
                response['message'] = self.inactivemessage
        response['outplayers'] = outplayerlist
    
        hand = []
        if playerindex is not None:
            for card in self.players[playerindex].hand.cards:
                hand.append({'suit': card.suit, 'number': card.number, 
                            'eligible': card.eligible, 'id': card.id})
        response['hand'] = hand
        return response

    def render_whist_data(self):
        response = {}
        response['name'] = self.name
        response['code'] = self.code
        response['state'] = self.state
        response['trumps'] = self.trumps
        response['currentsuit'] = self.currentsuit
        response['currentround'] = self.currentround
        response['activemessage'] = self.activemessage
        response['inactivemessage'] = self.inactivemessage
        response['inactivemessage'] = self.inactivemessage
        response['tiebreak'] = self.tiebreak
        return response
        

    def render_player(self, playerid):
        for player in self.all_players:
            if player.id == playerid:
                response = {}
                response['number'] = player.number
                response['id'] = player.id
                response['sessionid'] = player.sessionid
                response['name'] = player.name
                response['active'] = player.active
                response['connected'] = player.connected
                response['admin'] = player.admin
                response['tricks'] = player.tricks
                response['hand'] = player.hand
                response['colour'] = player.colour
                response['calling_trumps'] = player.calling_trumps
                response['playedcard'] = player.playedcard
                response['wontrick'] = player.wontrick
                response['start_turn_with'] = player.start_turn_with
                response['ingame'] = player.ingame
                response['dealer'] = player.dealer
        return response
