# Knockout Whist

This was built during Covid lockdowns using libraries from the time, which are now been outdated.  Specifically, 
it uses a javscript websocket client that requires very specific and finicky python package versions to have a compatable server.  It was originally hosted on Heroku, but they ended the free tier and depreciated support for the database version.  So it has now been converted into docker containers - one for the app, and one for redis.

# Setting it up manually 

On any system with docker:

Download the git repo `git clone https://gitlab.com/maxdobson/whist.git`

Change into that directory.

Create network: `docker network create -d bridge whist-network`

Launch redis container: `docker run --network=whist-network -itd --rm --name=redistest redis:5.0-alpine`

Build app container `docker build -t whistapp .`

Run app container `docker run --network=whist-network -itd -p 80:5000 --rm --name=app whistapp`

Currently the supplied docker compose file does not work.

To clean up afterwards:

`docker stop whistapp`
`docker stop redis`
`docker network rm whist-network`

Other useful commands

List running containers: `docker ps`
list running and stopped containers: `docker ps -a`
list networks: `docker network ls`
Bash into container: `docker exec -it <container> /bin/bash`

## How it works:

For development purposes:

 - Flask backend, which operates the websockets, stores data and sessions in Redis, handles the game logic, and serves up the built VueJs static files.  You can run the backend with `python app.py`
 - Vuejs front end single page application.  Used the Vue CLI project structure (e.g. mainly in `src`).  Used `npm run build` to generate the finished static files in `\dist`.

The codebase has now been updated to docker.



## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).



certifi==2023.7.22
chardet==3.0.4
click==8.0.4
dataclasses==0.8
dnspython==1.16.0
eventlet==0.25.2
Flask==2.0.3
flask-redis==0.4.0
Flask-Session==0.3.1
Flask-SocketIO==4.3.0
greenlet==2.0.2
gunicorn==21.2.0
idna==2.10
importlib-metadata==4.8.3
itsdangerous==2.0.1
Jinja2==3.0.3
MarkupSafe==2.0.1
monotonic==1.6
packaging==21.3
pyparsing==3.1.0
python-engineio==3.14.2
python-socketio==4.5.1
redis==3.5.2
requests==2.23.0
six==1.16.0
typing_extensions==4.1.1
urllib3==1.25.11
Werkzeug==2.0.3
zipp==3.6.0