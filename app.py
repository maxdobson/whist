# ------
# app.py
# ------
# The main entry point for the application backend.

import eventlet
eventlet.monkey_patch()

from flask import Flask, render_template, request, session, redirect, url_for
from flask_socketio import SocketIO, emit, join_room, leave_room
from flask_session import Session
from flask_redis import FlaskRedis
from game import Player, Deck, Whist
import json
import random
import pickle
import os
import requests
import uuid

import socket


print('ATTEMPT *************')
# Set Flask app to serve static files and templates from vuejs static files
app = Flask(__name__, static_url_path='', 
            static_folder='dist',
            template_folder='dist')

# Set secret key, needed for sessions.
app.config['SECRET_KEY'] = os.environ.get('SECRET_KEY') or 'secret!'

# Setup Redis, which is used for server-side sessions and saving game data.
app.config['REDIS_URL'] = os.environ.get('REDIS_URL') or "redis://redistest:6379"
print('----------------------')
print(os.environ.get('REDIS_URL'))
print('----------------------')
print('gettting app')
print(socket.gethostbyname('app'))
print('-----------------------------')

app.config['SESSION_TYPE'] = 'redis'
r = FlaskRedis(app)
app.config['SESSION_REDIS'] = r
r.set('test', 'It is workings', ex=7200)
print(r.get('test'))

# Setup sessions.  Allow CORS for development purposes (e.g. separate front and back end servers)
Session(app)
socketio = SocketIO(app, manage_session=False, cors_allowed_origins="*")


# Helper functions
#------------------

def save_gamedata(code, gameobject):
    # Saves game data to redis
    pickled_game = pickle.dumps(gameobject)
    r.set(code, pickled_game, ex=7200) # Keep games for two hours only.

def load_gamedata(code):
    # Loads game data from Redis
    unpacked_object = pickle.loads(r.get(code))
    return unpacked_object

def broadcast_individual_updates(gameobject):
    # Sends every player the relevant gameupdate data
    for player in gameobject.players:
        emit('GAMEUPDATE', json.dumps(gameobject.render_dict(player.id)),
            room=player.id)
    for player in gameobject.outplayers:
        emit('GAMEUPDATE', json.dumps(gameobject.render_dict(player.id)),
            room=player.id)

# Routes
#------------------

@app.route('/')
def index():
    # Ensures a unique session id, and serves the static index file
    if 'id' not in session:
        session['id'] = uuid.uuid1()
    return render_template("index.html")

@app.route('/reset')
def reset():
    # Used to clear a session and load homepage (i.e. exit a game)
    session.clear()
    return redirect(url_for('index'))


@app.route('/debug/<roomcode>')
def debug(roomcode):
    # Provides a debugging page which shows data on the game and for each player.
    # Could allow someone to cheat - could add login required in future.
    whist = load_gamedata(roomcode.upper())
    debugdata = whist.render_whist_data()
    playerdebug = []
    for player in whist.players:
        current_player = {}
        current_player['inorout'] = 'in'
        current_player['server'] = whist.render_player(player.id)
        current_player['gameupdate'] = whist.render_dict(player.id)
        playerdebug.append(current_player)
    for player in whist.outplayers:
        current_player = {}
        current_player['inorout'] = 'out'
        current_player['server'] = whist.render_player(player.id)
        current_player['gameupdate'] = whist.render_dict(player.id)
        playerdebug.append(current_player)
    return render_template("debug.html", debugdata=debugdata, dubugplayers=playerdebug)

# Websocket events
# ------------------

@socketio.on('connect')
def connect():
    # Handles the websocket initial connection or reconnection
    # If there is a valid code (room) associated with the session, send a game update to put the
    # client back in the game, else delete the room association, warn, and start from scratch.
    sessioncode = session.get('code')
    roomfound = False
    if sessioncode is not None:
        rooms = [x.decode("utf-8") for x in r.keys()] # Read keys from Redis
        if sessioncode in rooms:
            roomfound = True
        else:
            session.pop("code", None)
            emit('JOINFAILED', json.dumps({'code': sessioncode}))
    if roomfound:
        whist = load_gamedata(session['code'])
        whist.update_socket_from_session(session['id'], request.sid)
        save_gamedata(session['code'], whist)
        emit('GAMEUPDATE', json.dumps(whist.render_dict(request.sid)),
                room=request.sid)
    else:
        # Normal connection
        emit('my response', {'data': 'Connected'})


@socketio.on('disconnect')
def stub_disconnect():
    # This is currently not used.  Could be used as a hook in future to track disconnections
    # and update the game accordingly.
    print('*disconnect*{}'.format(request.sid))


@socketio.on('createRoom')
def createroom(message):
    # Handles event sent when a client clicks the "Host new game" button.
    # Creates room, sets up the game, and adds the player to it.
    data = json.loads(message)
    username = data['username']
    userid = data['userid']
    rooms = [x.decode("utf-8") for x in r.keys()]
    roomcreated = False
    while roomcreated == False: # Used to try another code if the randomly chosen one already exists.
        code = ''.join(random.choices('ACEFHJKLMNPRTWXY', k=4))
        if code not in rooms:
            drawpile = Deck()
            drawpile.gen_standard_deck()
            whist = Whist(code=code, state='lobby', currentround=0,
              drawpile=drawpile)
            roomcreated=True
            emit('ROOM_CREATED', json.dumps({'code': code}))
            print('Created room: {}'.format(code))
    whist.add_player(Player(userid, username, admin=True, active=True, sessionid=session['id']))
    whist.activemessage = 'Start when ready'
    whist.inactivemessage = 'Waiting for {} to start game'.format(whist.get_active_playername())
    session['code'] = code
    join_room(code) # This joins the client to the socketio room, which uses the same code as the server code.
    # Todo: Check if NEWJOINER event is needed, otherwise delete and just use GAMEUPDATE for simplicity.
    emit('NEWJOINER', json.dumps(whist.render_dict(userid)),
         room=code, broadcast=True)
    save_gamedata(code, whist)

@socketio.on('joinRoom')
def joinroom(message):
    # Handles event when a client requests to join a game room by providing a code.
    data = json.loads(message)
    code = data['code'].upper()
    username = data['username']
    userid = data['userid']
    rooms = [x.decode("utf-8") for x in r.keys()]
    if code in rooms:
        whist = load_gamedata(code)

        # If the game is in the lobby, add the player normally. Otherwise it is in progress, so add them to
        # the list of players who are out of the game.
        if whist.state != 'lobby':
            emit('TOASTMESSAGE', json.dumps({'message': 'Game in progress', 'style': 'is-warning'}))
            whist.add_player(Player(userid, username, admin=False, active=False, sessionid=session['id']), watching=True)
        else:
            whist.add_player(Player(userid, username, admin=False, active=False, sessionid=session['id']))
        join_room(code)
        save_gamedata(code, whist)
        session['code'] = code
        broadcast_individual_updates(whist)
    else:
        # If code not found, inform the client.
        emit('JOINFAILED', json.dumps({'code': code}))


@socketio.on('startGame')
def start_game(message):
    # Handles when the admin starts a game for the first time from the lobby.
    data = json.loads(message)
    code = data['code'].upper()
    whist = load_gamedata(code)
    whist.drawpile.shuffle()
    whist.next_round()
    whist.activemessage = 'Start the hand'
    whist.inactivemessage = '{} to play'.format(whist.get_active_playername())
    save_gamedata(code, whist)
    broadcast_individual_updates(whist)        

@socketio.on('refreshGameData')
def refreshgamedata(message):
    # Todo - DONT THINK THIS IS USED - DELETE? Intended t send one player an update, but use send all instead?
    data = json.loads(message)
    code = data['code'].upper()
    playerid = data['id']    
    whist = load_gamedata(code)
    emit('GAMEUPDATE', json.dumps(whist.render_dict(playerid)),
        room=playerid)

@socketio.on('playedCard')
def played_card(message):
    # Handles event when a client plays a card
    data = json.loads(message)
    code = data['code'].upper()
    whist = load_gamedata(code)
    userid = data['userid']
    cardid = data['cardid']
    whist.card_played(userid, cardid)
    save_gamedata(code, whist)
    broadcast_individual_updates(whist)

@socketio.on('collectTrick')
def collectTrick(message):
    # Handles event when a client collects a trick
    data = json.loads(message)
    code = data['code'].upper()
    whist = load_gamedata(code)
    userid = data['userid']
    whist.next_trick(userid)
    save_gamedata(code, whist)
    broadcast_individual_updates(whist)

@socketio.on('dealCards')
def deal_cards(message):
    # Handles event when a client deals cards at the beginning of a new round.
    data = json.loads(message)
    code = data['code'].upper()
    whist = load_gamedata(code)
    userid = data['userid']
    whist.clear_previous_hand()
    save_gamedata(code, whist)
    whist.render_all_dicts()
    broadcast_individual_updates(whist)


@socketio.on('selectedTrumps')
def selectedTrumps(message):
    # Handle event when a client chooses trumps.
    data = json.loads(message)
    code = data['code'].upper()
    trumps = data['suit']
    whist = load_gamedata(code)
    whist.set_trumps(trumps)
    save_gamedata(code, whist)
    broadcast_individual_updates(whist)

@socketio.on('reStartGame')
def restart_game(message):
    # Handles the restart event (when one game has finished, and the admin starts a new one.)
    # Rather than use the existing game instance, create a new one (to minimise the risk of bugs).
    # Adds the players back based on the initally required information only.
    data = json.loads(message)
    code = data['code'].upper()
    oldwhist = load_gamedata(code)
    allplayers = []
    for player in oldwhist.players:
        allplayers.append({'name': player.name, 'id': player.id, 'admin': player.admin, 'sessionid': player.sessionid})
    for player in oldwhist.outplayers:
        allplayers.append({'name': player.name, 'id': player.id, 'admin': player.admin, 'sessionid': player.sessionid})
    drawpile = Deck()
    drawpile.gen_standard_deck()
    newwhist = Whist(code=code, state='lobby', currentround=0,
        drawpile=drawpile)       
    for player in allplayers:
        newwhist.add_player(Player(player['id'], player['name'], admin=player['admin'], sessionid=player['sessionid']))
    newwhist.drawpile.shuffle()
    newwhist.next_round()
    newwhist.activemessage = 'Start the hand'
    newwhist.inactivemessage = '{} to play'.format(newwhist.get_active_playername())
    save_gamedata(code, newwhist)
    broadcast_individual_updates(newwhist)

if __name__ == '__main__':
    socketio.run(app, debug='DYNO' not in os.environ, host='0.0.0.0') # Effectively turns debug mode off if platform is Heroku.
    print('running')