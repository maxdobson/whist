# syntax=docker/dockerfile:1
FROM python:3.6-alpine
WORKDIR /code
#ENV REDIS_URL redis://redistest:6379
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 5000
COPY . .
CMD ["python", "app.py"]