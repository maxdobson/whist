echo off
rem Because the build process deletes all files in the dist directory
rem when rebuilding, need to copy across any additional templates
rem desired for Flask to serve (after each build).  This batch file is to 
rem reduce the effort. involved.
echo on
copy "templates\debug.html" "dist\debug.html"